# Pixel Game Jam 2020 - Pixels

> A small playable game with homemade art!

Welcome to the Game jam!

## Rules

  * Pixels must be involved

  * Should function in a browser, click a link, play a game

  * Create your own sound and graphics media, license free

    .. or find someone to do the work for you!

  * Invite friends

  * Come up with more rules!

  * Share code via email or pull request on Gitlab

### Finish times

Game creation time finishes:

   2 Jan. 2021 23:59:59

Game play-through time finishes:

  10 Jan. 2021 23:59:59

